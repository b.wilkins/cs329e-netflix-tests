#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, truncate
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3\n3.2\n3.5\n0.85\n")

    def test_eval_2(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.6\n3.5\n3.6\n4.2\n3.6\n3.7\n3.5\n3.6\n3.8\n3.6\n3.1\n3.6\n3.9\n3.8\n3.2\n3.9\n3.7\n4.1\n3.9\n3.9\n3.3\n4.3\n3.5\n0.83\n")


    def test_eval_3(self):
        r = StringIO("10037:\n253214\n612895\n769764\n396150\n2555464\n1988133\n560277\n1839686\n2569369\n1845071\n1627613\n948108\n1316833\n911710\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10037:\n3.7\n3.2\n3.4\n3.7\n3.6\n3.6\n3.3\n3.1\n3.4\n3.6\n3.4\n3.4\n3.4\n3.4\n0.73\n")

    # ---
    # truncate
    # -----
    def test_truncate_1(self):
        num = 4.345
        dec = 2
        newnum = truncate(num, dec)
        self.assertEqual(newnum, 4.34)
    
    def test_truncate_2(self):
        num = .567
        dec = 1
        newnum = truncate(num, dec)
        self.assertEqual(newnum, .5)
    
    def test_truncate_3(self):
        num = 4.7837836
        dec = 1
        newnum = truncate(num, dec)
        self.assertEqual(newnum, 4.7)
    
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
