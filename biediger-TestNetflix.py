#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----
    #GET RMSE TEST CASES:
    ##print(ACTUAL_CUSTOMER_RATING.keys())
    #Make sure it is ok to be returning RMSE in netflix_eval()

    def test_rmse_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        self.assertLess(netflix_eval(r, w), 1.0)

    def test_rmse_2(self):
        r = StringIO("7635:\n2017298\n33:\n915257\n533:\n42123\n6615:\n739018")
        w = StringIO()
        self.assertLess(netflix_eval(r, w), 1.0)

    def test_rmse_3(self):
        r = StringIO("10027:\n2269919\n148:\n1988954\n12999:\n1512842\n3917:\n424128")
        w = StringIO()
        self.assertLess(netflix_eval(r, w), 1.0)

    def test_output(self):
        r = StringIO("10027:\n2269919\n148:\n1988954\n12999:\n1512842\n3917:\n424128")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(w.getvalue(), "10027:\n3.7\n148:\n3.3\n12999:\n3.6\n3917:\n4.0\n0.72\n" )
# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
